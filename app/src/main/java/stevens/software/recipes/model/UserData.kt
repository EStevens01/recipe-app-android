package stevens.software.recipes.model

data class UserData(val connection: String, val clientId: String, val email: String, val password: String, val name: String) {
}


package stevens.software.recipes.model

data class MethodData(val methodId: Integer, val stepNumber: Integer, val step: String) {
}
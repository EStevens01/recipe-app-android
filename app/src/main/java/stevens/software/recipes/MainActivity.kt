package stevens.software.recipes

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.app_main)
        val loginButton = findViewById<Button>(R.id.logout)
        loginButton.setOnClickListener { logout() }

        val accessToken =
            intent.getStringExtra("com.auth0.ACCESS_TOKEN")
        val textView = findViewById<TextView>(R.id.creds)
        textView.text = accessToken

    }

    private fun logout() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("com.auth0.CLEAR_CREDENTIALS", true)
        startActivity(intent)
        finish()
    }
}

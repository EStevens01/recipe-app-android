package stevens.software.recipes


import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.auth0.android.Auth0
import com.auth0.android.Auth0Exception
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.authentication.storage.CredentialsManagerException
import com.auth0.android.authentication.storage.SecureCredentialsManager
import com.auth0.android.authentication.storage.SharedPreferencesStorage
import com.auth0.android.callback.BaseCallback
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials

class LoginActivity : AppCompatActivity() {

    private lateinit var account: Auth0

    val EXTRA_CLEAR_CREDENTIALS = "com.auth0.CLEAR_CREDENTIALS"
    val EXTRA_ACCESS_TOKEN = "com.auth0.ACCESS_TOKEN"
    val EXTRA_ID_TOKEN = "com.auth0.ID_TOKEN"
    private val CODE_DEVICE_AUTHENTICATION = 22

    private var credentialsManager: SecureCredentialsManager? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        account = Auth0(getString(R.string.clientId), getString(R.string.domain))
        account.isOIDCConformant = true;

        credentialsManager = SecureCredentialsManager(
            this,
            AuthenticationAPIClient(account),
            SharedPreferencesStorage(this)
        )

//        credentialsManager!!.requireAuthentication(this, CODE_DEVICE_AUTHENTICATION, getString(R.string.request_credentials_title), null);

        val loginButton: Button = findViewById(R.id.button)
        loginButton.setOnClickListener {
            login()
        }


        // Check if the activity was launched to log the user out
        if (intent.getBooleanExtra(EXTRA_CLEAR_CREDENTIALS, false)) {
            logout();
            return;
        }

        if (credentialsManager!!.hasValidCredentials()) {
            // Obtain the existing credentials and move to the next activity
            showNextActivity();
        }

        //Check if the activity was launched to log the user out
        if (intent.getBooleanExtra(EXTRA_CLEAR_CREDENTIALS, false)) {
            logout()
        }
    }

    private fun login() {
        WebAuthProvider.login(account)
            .withScheme("demo")
            .withScope("openid offline_access")
            .withAudience(
                String.format(
                    "https://%s/userinfo",
                    "dev-mq4txwco.us.auth0.com"
                )
            )
            .start(this@LoginActivity, loginCallback)

        }

    private val loginCallback: AuthCallback = object : AuthCallback {
        override fun onFailure(dialog: Dialog) {
            runOnUiThread { dialog.show() }
        }

        override fun onFailure(exception: AuthenticationException) {
            runOnUiThread {
                Toast.makeText(
                    this@LoginActivity,
                    "Log In - Error Occurred",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onSuccess(credentials: Credentials) {
            credentialsManager!!.saveCredentials(credentials)
            showNextActivity()
        }
    }

    private fun logout() {
        WebAuthProvider.logout(account)
            .withScheme("demo")
            .start(this,  logoutCallback)


    }

    val logoutCallback: VoidCallback = object : VoidCallback {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onSuccess(payload: Void?) {
            credentialsManager!!.clearCredentials()
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onFailure(error: Auth0Exception) {
            showNextActivity()
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun showNextActivity() {
        credentialsManager!!.getCredentials(object :
            BaseCallback<Credentials?, CredentialsManagerException?> {

            override fun onSuccess(payload: Credentials?) {
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                intent.putExtra(EXTRA_ACCESS_TOKEN, payload?.accessToken)
                intent.putExtra(EXTRA_ID_TOKEN, payload?.idToken)
                startActivity(intent)
                finish()
            }

            override fun onFailure(error: CredentialsManagerException) {
                finish()
            }
        })
    }
}


